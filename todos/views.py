from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList
from .forms import TodoForm
# Create your views here.


def todo_list_list(request):
    todo_lists = TodoList.objects.all()
    context = {
        "todo_list_list": todo_lists,
    }
    return render(request, "todos/main_page.html", context)


def todo_list_detail(request, id):
    todo_item = get_object_or_404(TodoList, id=id)
    context = {
        "list_details": todo_item,
    }
    return render(request, "todos/details.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def todo_list_edit(request, id):
    edit = get_object_or_404(TodoList, id=id)

    if request.method == "POST":
        form = TodoForm(request.POST, instance=edit)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=edit.id)
    else:
        form = TodoForm(instance=edit)

    context = {
        "form": form,
    }
    return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
    this_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        this_list.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")
